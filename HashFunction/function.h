#ifndef _FUNCTION_H
#define _FUNCTION_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAXIP = 16

static unsigned int Out[225746];      //存放32位哈希算法得到的哈希值
static unsigned short OutPut[225746]; //存放16位哈希算法得到的哈希值

struct trace
{
	char sourceIp[16];
	char destinationIp[16];
	char sourcePort[6];
	char destinationPort[6];
} record[300000];

 unsigned int iptounint(char* str) // 将字符串类型的ip地址输出为无符号整数
{
	 int i = 0;
	 int j = 0;
     unsigned int ipint;
	 char new_str[3];
	 int num = 0;
    int result[4];
	while(*str != '\0')
	{
		while((*str != '.') && (*str != '\0'))
		{
			new_str[i] = *str;
			num = num * 10 + new_str[i] - '0';
			str += 1;
			i += 1;
		}
		result[j] = num;
		num = 0;
		if (*str == '\0')
		{
			break;
		}
		else
		{
			str += 1;
			i = 0;
			j += 1;	
		}	
	}
    ipint=(result[0]*256*256*256)+(result[1]*256*256)+(result[2]*256)+result[3];
    return ipint;
}

 unsigned short iptounshort1(char* str) // 将字符串类型的ip地址的前16位输出为无符号整数
{
	 int i = 0;
	 int j = 0;
     unsigned short ipshort1;
	char new_str[3];
	 int num = 0;
    static int result[4];
	while(*str != '\0')
	{
		while((*str != '.') && (*str != '\0'))
		{
			new_str[i] = *str;
			num = num * 10 + new_str[i] - '0';
			str += 1;
			i += 1;
		}
		result[j] = num;
		num = 0;
		if (*str == '\0')
		{
			break;
		}
		else
		{
			str += 1;
			i = 0;
			j += 1;	
		}	
	}
    ipshort1=(result[0]*256)+result[1];
    return ipshort1;
}

 unsigned short iptounshort2(char* str) // 将字符串类型的ip地址的后16位输出为无符号整数
{
	 int i = 0;
	 int j = 0;
     unsigned short ipshort2;
	 char new_str[3];
	 int num = 0;
     int result[4];
	while(*str != '\0')
	{
		while((*str != '.') && (*str != '\0'))
		{
			new_str[i] = *str;
			num = num * 10 + new_str[i] - '0';
			str += 1;
			i += 1;
		}
		result[j] = num;
		num = 0;
		if (*str == '\0')
		{
			break;
		}
		else
		{
			str += 1;
			i = 0;
			j += 1;	
		}	
	}
    ipshort2=(result[2]*256)+result[3];
    return ipshort2;
}

void Average32BitEntropy(void)//计算所有返回值类型为32比特位数的位熵，并取平均值
{
	float count=0;
	float sum,AverageBE;
	for (int i = 0; i < (sizeof(Out)/sizeof(Out[0])); i++)
	{
		if (Out[i]==0)
		{continue;}
		sum += BitEntropy32(Out[i]);
		count++;
	}
	AverageBE=(float)(sum/count);
	printf("The average BitEntropy is %f\n",AverageBE);
}

void Average16BitEntropy(void)//计算所有返回值类型为16比特位数的位熵，并取平均值
{
	float count=0;
	float sum,AverageBE;
	for (int i = 0; i < (sizeof(OutPut)/sizeof(OutPut[0])); i++)
	{
		if (OutPut[i]==0)
		{continue;}
		sum += BitEntropy16(OutPut[i]);
		count++;
	}
	AverageBE=(float)(sum/count);
	printf("The average BitEntropy is %f\n",AverageBE);
}

void ConflictRate32(void)//计算返回值类型为32比特位数的冲突率
{
	int countArray[sizeof(Out)/sizeof(Out[0])];//构建一个与存储哈希值的数组长度相同的数组
	int j,count;
	double num = sizeof(Out)/sizeof(Out[0]);
	double number=0;
	double value;
	for (int i = 0; i < num ; i++)//对新数组初始化，数组每个值初始化为-1
	{
		j=i;
		if (Out[i]==0)
		{countArray[j]=-2;}
		else
		{countArray[j]=-1;}
	}
	
    for(int i = 0; i < num ; i++) 
	{
		if(Out[i]==0){continue;}//不统计数组中哈希值为0的情况
		else count=1;//出现的数字频率至少为1
		
        if (countArray[i] == 0) {continue;} // 值为0表示该数字已经被统计过

        for(j = i+1; j < num; j++) 
		{  
            if(Out[i]==Out[j]) 
			{
                countArray[j] = 0;    
                count++;
            }  
        }  
		
        countArray[i] = count; //记录inputArray[i]出现的频率
    }  
	
    for (int i = 0; i < num; i++)
	{
		if ((countArray[i] != 0)&&(countArray[i] != -2)&&(countArray[i]>1))//此情况已经发生冲突
		value += ((countArray[i])*(countArray[i]+1))/2;
		if(countArray[i]==1)
		number += 1;
	}
	value = (2/((num)*(num+1)))*(value+number);
	printf("The ConflictRate is %e\n",value);
    
}
	
void ConflictRate16(void)//计算返回值类型为16比特位数的冲突率
{
	int countArray[sizeof(OutPut)/sizeof(OutPut[0])];//构建一个与存储哈希值的数组长度相同的数组
	int j,count;
	double num = sizeof(Out)/sizeof(Out[0]);
	double number = 0;
	double value;
	for (int i = 0; i < num ; i++)//对新数组初始化，数组每个值初始化为-1
	{
		j=i;
		if (OutPut[i]==0)
		{countArray[j]=-2;}
		else
		{countArray[j]=-1;}
	}
	
    for(int i = 0; i < num ; i++) 
	{
		if(OutPut[i]==0){continue;}//不统计数组中哈希值为0的情况
		else count=1;//出现的数字频率至少为1
		
        if (countArray[i] == 0) {continue;} // 值为0表示该数字已经被统计过

        for(j = i+1; j < num; j++) 
		{  
            if(OutPut[i]==OutPut[j]) 
			{
                countArray[j] = 0;    
                count++;
            }  
        }  
		
        countArray[i] = count; //记录inputArray[i]出现的频率
    }  
	for (int i = 0; i < num; i++)
	{
		if ((countArray[i] != 0)&&(countArray[i] != -2)&&(countArray[i]>1))//此情况已经发生冲突
		value += ((countArray[i])*(countArray[i]+1))/2;
		if(countArray[i]==1)
		number += 1;
	}
	value = (2/((num)*(num+1)))*(value+number);
	printf("The ConflictRate is %e\n",value);
}

void Initialize(void)  //打开读取数据文件并进行hash算法，将得到的hash值存储在数组里
{
	int ch;
	FILE *fp;
	unsigned long idx = 0;
	
	if ((fp = fopen("./data.csv", "r")) == NULL)
	{
		exit(EXIT_FAILURE);
	}

	int record_idx = 1, comma_cnt = 0, tmp_idx = 0;
	while ((ch = getc(fp)) != EOF)
	{
		
		if (ch == '\r')
			continue;
		else if (ch == '\n')
		{
			record_idx++;
			tmp_idx = 0;
			comma_cnt = 0;
		}
		else if (ch == ',')
		{
			comma_cnt++;
			tmp_idx = 0;
		}
		else
		{
			if (comma_cnt == 0)
			{
				record[record_idx].sourceIp[tmp_idx++] = ch;
			}
			else if (comma_cnt == 1)
			{
				record[record_idx].destinationIp[tmp_idx++] = ch;
			}
			else if (comma_cnt == 2)
			{
				record[record_idx].sourcePort[tmp_idx++] = ch;
			}
			else if (comma_cnt == 3)
			{
				record[record_idx].destinationPort[tmp_idx++] = ch;
			}
		}
	}

    //printf("%s, %s, %s, %s \n", record[3].sourceIp, record[3].destinationIp, record[3].sourcePort, record[3].destinationPort);//测试用
	
	for (int i = 2; i < record_idx; i++)
	{
		/*unsigned int a[3];  //用于IPSX哈希函数的输入
		a[0]=iptounint(record[i].sourceIp);
		a[1]=iptounint(record[i].destinationIp);
		a[2]=(atoi(record[i].sourcePort))*256 + atoi(record[i].destinationPort);

		Out[i-1]=ipsx(a[0],a[1],a[2]);
		*/

		/*unsigned short b[6];  //用于xor哈希函数的输入
		
		b[0]=iptounshort1(record[i].sourceIp);
		b[1]=iptounshort2(record[i].sourceIp);
		b[2]=iptounshort1(record[i].destinationIp);
		b[3]=iptounshort2(record[i].destinationIp);
		b[4]=(unsigned short)atoi(record[i].sourcePort);
		b[5]=(unsigned short)atoi(record[i].destinationPort);

		Out[i-1]=xorhash(b[0],b[1],b[2],b[3],b[4],b[5]);
		*/
		
		
	    char s[50];  //用于bobhash、crc32hash、BKDRhash、APhash的输入；
		strcpy(s,record[i].sourceIp);
		strcat(s,record[i].destinationIp);
		strcat(s,record[i].sourcePort);
		strcat(s,record[i].destinationPort);
		int size=sizeof(s);

		

		//Out[i-1]=APHash(s);
		//Out[i-1]=BKDRHash(s);
		Out[i-1]=bob_hash(s,size,1);
        //Out[i-1]=crc32(s,size); 

		//printf("%d %u\n",i-1,Out[i-1]);
	}
	fclose(fp);
	printf("Fild read complete!\n");
	//printf("This Hashfuntion is IPSX\n");
	//printf("This Hashfuntion is XOR\n");
	//printf("This Hashfuntion is APhash\n");
	//printf("This Hashfuntion is BKDRhash\n");
	printf("This Hashfuntion is BOB\n");
	//printf("This Hashfuntion is CRC32\n");
}

#endif

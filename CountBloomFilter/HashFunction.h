#ifndef HASHFUNCTION_H_INCLUDED
#define HASHFUNCTION_H_INCLUDED

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "CBFStruct.h"
#include "Function.h"
/***************
command=0;查询
command=1;插入
command=2;删除
****************/
int HashFunctionNum = 0;//控制使用的hash函数个数

void GetHashFunctionNum(int num)
{
   HashFunctionNum = num;
}

int CheckBit(CountBloomFilter CBF, int Index)
{
    printf("%d,",Index);
    if(CBF.DictHash[Index].Bit == 1)
        return CBF.DictHash[Index].Count; //返回计数器值
    return 0; //不存在该元素
}

int Operation(CountBloomFilter CBF, int Index ,int command)
{
    if (command == 1){
        CBF.DictHash[Index].Bit = 1;
        CBF.DictHash[Index].Count++;
	}else if(command == 2){
	    int temp = --CBF.DictHash[Index].Count;
        if(temp == 0)
            CBF.DictHash[Index].Bit = 0;
	}else
        return CheckBit(CBF,Index);
    return 0;
}
/***********************************************
	函数名称: bool hash1(char* buffer, unsigned int*& dicthash, int command=0)
	函数功能: 将字符串按照ascii码变为数字，并得到其积，最后对比特长度取余，
               如果int参数为1则将bloomfilter对应标志位置1，否则跳过。之后查询该位置是否为1.
	传入参数: char* buffer--目标字符串，int command=0--查询，=1--更改
	返回值: 如果是1返回true，否则为false。
************************************************/
int hash1(char* buffer, CountBloomFilter CBF, int command)
{
    if(HashFunctionNum < 1)
        return 1;

	unsigned int tmp = 1;
	while (*buffer != '\0')
	{
	    //printf("111");
		tmp *= (int)(*buffer);
		tmp %= CBF.CBF_Bit_Count;
		buffer++;
	}
	return Operation(CBF, tmp % CBF.CBF_Bit_Count ,command);
}
/***********************************************
	函数名称: bool hash2(char* buffer, unsigned  int*& dicthash, int command = 0)
	函数功能: 将字符串按照ascii码变为数字，并得到其和，最后对比特长度取余，
               如果int参数为1则将bloomfilter对应标志位置1，否则跳过。之后查询该位置是否为1.
	传入参数: char* buffer--目标字符串，int command=0--查询，=1--更改
	返回值: 如果是1返回true，否则为false。
************************************************/
int hash2(char* buffer, CountBloomFilter CBF, int command )
{
    if(HashFunctionNum < 2)
        return 1;

	unsigned int tmp = 0;
	while (*buffer != '\0')
	{
		tmp += (int)(*buffer);

		buffer++;
	}
	//printf("222");
	return Operation(CBF, tmp % CBF.CBF_Bit_Count ,command);
}
/***********************************************
	函数名称: bool hash3(char* buffer, unsigned  int*& dicthash, int command = 0)
	函数功能: 将字符串按照ascii码变为数字，并得到平方根取整的和，最后对比特长度取余，
               如果int参数为1则将bloomfilter对应标志位置1，否则跳过。之后查询该位置是否为1.
	传入参数: char* buffer--目标字符串，int command=0--查询，=1--更改
	返回值: 如果是1返回true，否则为false。
************************************************/
int hash3(char* buffer, CountBloomFilter CBF, int command )
{
	if(HashFunctionNum < 3)
        return 1;

	unsigned int tmp = 1;
	while (*buffer != '\0')
	{
		tmp *= (int)(sqrt((double)*buffer));

		buffer++;
	}
	return Operation(CBF, tmp % CBF.CBF_Bit_Count ,command);
}
/***********************************************
	函数名称: bool hash34char* buffer, unsigned  int*& dicthash, int command = 0)
	函数功能: 将字符串按照ascii码变为数字，按+-+-。。。顺序依次加减，最后对比特长度取余，
               如果int参数为1则将bloomfilter对应标志位置1，否则跳过。之后查询该位置是否为1.
	传入参数: char* buffer--目标字符串，int command=0--查询，=1--更改
	返回值: 如果是1返回true，否则为false。
************************************************/
int hash4(char* buffer, CountBloomFilter CBF, int command )
{
    if(HashFunctionNum < 5)
        return 1;

	unsigned int tmp = 0;
	unsigned int sign = 1;
	while (*buffer != '\0')
	{
		tmp += sign*(int)(*buffer);
		sign *= -1;
		buffer++;
	}
	return Operation(CBF, tmp % CBF.CBF_Bit_Count ,command);
}

//BKDR Hash Function
int hash5(char* buffer, CountBloomFilter CBF, int command )
{
    if(HashFunctionNum < 5)
        return 1;

    unsigned int tmp = 0;
    unsigned int sign = 131;

    while(*buffer){
        tmp = tmp *sign + (*buffer++);
    }
    tmp = tmp & 0x7FFFFFFF;

    return Operation(CBF, tmp % CBF.CBF_Bit_Count ,command);
}

//AP Hash Function
int hash6(char* buffer, CountBloomFilter CBF, int command)
{
    if(HashFunctionNum < 6)
        return 1;

    unsigned int tmp = 0;
    int i;

    for(i = 0; *buffer; i++){
        if ((i & 1) == 0){
			tmp ^= ((tmp << 7) ^ (*buffer++) ^ (tmp >> 3));
		}
		else{
			tmp ^= (~((tmp << 11) ^ (*buffer++) ^ (tmp >> 5)));
		}
    }
    tmp = tmp & 0x7FFFFFFF;

    return Operation(CBF, tmp % CBF.CBF_Bit_Count ,command);
}

//DJB Hash function
int hash7(char* buffer, CountBloomFilter CBF, int command)
{
    if(HashFunctionNum < 7)
        return 1;

    unsigned int tmp = 5381;

    while (*buffer){
		tmp += (tmp << 5) + (*buffer++);
	}

	tmp = tmp & 0x7FFFFFFF;

	return Operation(CBF, tmp % CBF.CBF_Bit_Count ,command);
}

//JS Hash Function
int hash8(char* buffer, CountBloomFilter CBF, int command)
{
    if(HashFunctionNum < 8)
        return 1;

    unsigned int tmp = 1315423911;

	while (*buffer)
	{
		tmp ^= ((tmp << 5) + (*buffer++) + (tmp >> 2));
	}

	tmp = tmp & 0x7FFFFFFF;

	return Operation(CBF, tmp % CBF.CBF_Bit_Count ,command);
}

//RS Hash Function
int hash9(char* buffer, CountBloomFilter CBF, int command)
{
    if(HashFunctionNum < 9)
        return 1;

    unsigned int tmp = 0;
	unsigned int b = 378551;
	unsigned int a = 63689;

	while (*buffer)
	{
		tmp = tmp * a + (*buffer++);
		a *= b;
	}

	tmp = tmp & 0x7FFFFFFF;

	return Operation(CBF, tmp % CBF.CBF_Bit_Count ,command);
}

//SDBM Hash Function
int hash10(char* buffer, CountBloomFilter CBF, int command)
{
    if(HashFunctionNum < 10)
        return 1;

    unsigned int tmp = 0;

	while (*buffer)
	{
		// equivalent to: hash = 65599*hash + (*str++);
		tmp = (*buffer++) + (tmp << 6) + (tmp << 16) - tmp;
	}

	tmp = tmp & 0x7FFFFFFF;

	return Operation(CBF, tmp % CBF.CBF_Bit_Count ,command);
}

//FNV Hash Function
int hash11(char* buffer, CountBloomFilter CBF, int command)
{
    if(HashFunctionNum < 11)
        return 1;

    unsigned int tmp = 0;

	int fnvprime = 0x811C9DC5;

	while (*buffer) {
		tmp *= fnvprime;
		tmp ^= (int)(*buffer++);
	}

	tmp = tmp & 0x7FFFFFFF;

	return Operation(CBF, tmp % CBF.CBF_Bit_Count ,command);
}

//JAVA string hash function
int hash12(char* buffer, CountBloomFilter CBF, int command)
{
    if(HashFunctionNum < 12)
        return 1;

    unsigned int tmp = 0;

	while (*buffer) {
		tmp = tmp * 31 + (*buffer++);
	}

	tmp = tmp & 0x7FFFFFFF;

	return Operation(CBF, tmp % CBF.CBF_Bit_Count ,command);
}

/************
插入：
    添加元素的key需要根据k个哈希函数计算得到k个哈希值，
    根据哈希值将对应位置为1，并将计数器值加1
************/
int Insert(char * buffer)
{
    hash1(buffer, CBF, 1);
    hash2(buffer, CBF, 1);
    hash3(buffer, CBF, 1);
    hash4(buffer, CBF, 1);
    hash5(buffer, CBF, 1);
    hash6(buffer, CBF, 1);
    hash7(buffer, CBF, 1);
    hash8(buffer, CBF, 1);
    hash9(buffer, CBF, 1);
    hash10(buffer, CBF, 1);
    hash11(buffer, CBF, 1);
    hash12(buffer, CBF, 1);
    return 0;
}

int Delete(char * buffer)
{
    hash1(buffer, CBF, 2);
    hash2(buffer, CBF, 2);
    hash3(buffer, CBF, 2);
    hash4(buffer, CBF, 2);
    hash5(buffer, CBF, 2);
    hash6(buffer, CBF, 2);
    hash7(buffer, CBF, 2);
    hash8(buffer, CBF, 2);
    hash9(buffer, CBF, 2);
    hash10(buffer, CBF, 2);
    hash11(buffer, CBF, 2);
    hash12(buffer, CBF, 2);
    return 0;
}

int Query(char *buffer)
{
    int Count[13];
    Count[1] = hash1(buffer, CBF, 0);
    Count[2] = hash2(buffer, CBF, 0);
    Count[3] = hash3(buffer, CBF, 0);
    Count[4] = hash4(buffer, CBF, 0);
    Count[5] = hash5(buffer, CBF, 0);
    Count[6] = hash6(buffer, CBF, 0);
    Count[7] = hash7(buffer, CBF, 0);
    Count[8] = hash8(buffer, CBF, 0);
    Count[9] = hash9(buffer, CBF, 0);
    Count[10] = hash10(buffer, CBF, 0);
    Count[11] = hash11(buffer, CBF, 0);
    Count[12] = hash12(buffer, CBF, 0);

    int flag = 0;
    for(int i = 1; i <= 12; i++){
        if(Count[i] == 0)
            flag = 1;
    }
    if(flag == 1)//不存在该元素
        printf("Element does not exist!\n");
    else
        printf("Element exists!\n");
    return 0;
}

int ReadFile_Insert(char * File)
{
    FILE* fp = NULL;
    fp = fopen(File, "r");
    if (fp != NULL)
        printf("File Open Successful!!\n");
    else
        printf("File Open Failed!!\n");
    char row[1024];
    char *token;
    int Num=0;
    while (fgets(row, 1024, fp) != NULL){
        //printf("Row: %s", row);
        Num++;
        token = strtok(row, ",");
        //printf("Token: %s\n", token);
        Insert(token);
    }
    printf("Element insertion completed! %d pieces of data in total!\n",Num);
    return 0;
}

int ReadFile_Query(char * File)
{
    FILE* fp = NULL;
    fp = fopen(File, "r");
    if (fp != NULL)
        printf("File Open Successful!!\n");
    else
        printf("File Open Failed!!\n");
    char row[1024];
    char *token;
    while (fgets(row, 1024, fp) != NULL){
        //printf("Row: %s", row);
        token = strtok(row, ",");
        printf("Token: %s\n", token);
        Query(token);
    }
    printf("Element query completed!\n");
    return 0;
}

int ReadFile_Delete(char * File)
{
    FILE* fp = NULL;
    fp = fopen(File, "r");
    if (fp != NULL)
        printf("File Open Successful!!\n");
    else
        printf("File Open Failed!!\n");
    char row[1024];
    char *token;
    while (fgets(row, 1024, fp) != NULL){
        //printf("Row: %s", row);
        token = strtok(row, ",");
        printf("Token: %s\n", token);
        Delete(token);
    }
    printf("Element deletion completed!\n");
    return 0;
}


#endif // HASHFUNCTION_H_INCLUDED

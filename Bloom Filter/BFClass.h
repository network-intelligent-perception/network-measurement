#ifndef BFCLASS_H_INCLUDED
#define BFCLASS_H_INCLUDED
#define MAXLEN 64
#include "HashFuction.h"

typedef struct BloomFilter{
    int* dicthash;
    int BloomFilterBitCount;
    int HashNumber;
}BloomFilter;

BloomFilter BF;

int InitBF()
{
    printf("Please enter Bloom Filter size:");
    scanf("%d",&BF.BloomFilterBitCount);
    printf("Please enter the number of hash functions:");
    scanf("%d",&BF.HashNumber);
    int Size = BF.BloomFilterBitCount;
    //printf("Size:%d\n",Size);
    BF.dicthash = (int*)malloc(sizeof(int)*Size);
    if(BF.dicthash!=NULL)
        printf("Successful!\n");
    memset(BF.dicthash,0,Size);
    return 0;
}

int Destory()
{
    free(BF.dicthash);
    return 0;
}

void PrintBFCount()
{
    for(int i=1;i<=BF.BloomFilterBitCount;i++){
        printf("%d ",*(BF.dicthash+i));
        if(i%10==0)
            printf("\n");
    }

}

#endif // BFCLASS_H_INCLUDED
